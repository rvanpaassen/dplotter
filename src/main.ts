// https://secdevops.ai/weekend-project-part-4-integrating-websockets-into-a-real-time-vue-app-with-socket-io-and-vuex-e358e04f477c
// uses webpack
// https://blog.logrocket.com/full-guide-to-using-font-awesome-icons-in-vue-js-apps-5574c74d9b2d

// base software tools
import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faThumbsUp } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { Drag, Drop } from 'vue-drag-drop'

// application
import App from './App.vue'
import store from '@/store/Store.vue'
// store.commit('updateBaseURL', 'ws://127.0.0.1:8001/')

library.add(faThumbsUp)
Vue.use(BootstrapVue)
Vue.component('font-awesome-icon', FontAwesomeIcon)
Vue.component('drag', Drag)
Vue.component('drop', Drop)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  store: store,
  render: h => h(App)
}).$mount('#app')
