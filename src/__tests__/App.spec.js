import { mount, createLocalVue } from '@vue/test-utils'
import { Drag, Drop } from 'vue-drag-drop'
import BootstrapVue from 'bootstrap-vue'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faThumbsUp } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import App from '../App.vue'
import Vuex from 'vuex'
import store from '../store/Store.vue'
library.add(faThumbsUp)

test('App has a .center-content class', () => {
  const vue = createLocalVue()
  // const store = new Vuex.store()
  vue.use(Vuex)
  vue.component('drag', Drag)
  vue.component('drop', Drop)
  vue.use(BootstrapVue)
  vue.component('font-awesome-icon', FontAwesomeIcon)
  const app = mount(App, { vue, store })

  expect(app.classess()).toContain('center-content')
})
