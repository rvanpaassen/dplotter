import { shallowMount, mount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import LineTarget from '../components/LineTarget.vue'
import { Drag, Drop } from 'vue-drag-drop'
import BootstrapVue from 'bootstrap-vue'

var sinon = require('sinon')
const localVue = createLocalVue()
localVue.use(Vuex)
localVue.component('drag', Drag)
localVue.component('drop', Drop)
localVue.use(BootstrapVue)
// mount(LineTarget, { localVue })

describe('LineTarget', () => {
  let getters
  let actions
  let store

  beforeEach(() => {
    actions = {
      acceptLineX: sinon.fake(),
      acceptLineY: sinon.fake(),
      addLine: sinon.fake()
    }
    getters = {
      getLines: () => [ {
        linex: '',
        liney: ''
      } ],
      getCurrentEditPlotID: () => 1
    }
    store = new Vuex.Store({
      actions,
      getters
    })
  })

  it('renders correctly', () => {
    const wrapper = shallowMount(LineTarget, { store, localVue })
    expect(wrapper.html()).to.contain('New Line')
    // console.log(wrapper.html())
  })

  it('has a working add button', () => {
    const wrapper = mount(LineTarget, { localVue, store })
    wrapper.find('#add-line').trigger('click')
    expect(actions.addLine.should.have.been.called)
  })

  it('has a working x drop zone', () => {
    const wrapper = mount(LineTarget, { store, localVue })
    // console.log(wrapper.html())
    const acceptx = wrapper.find('#drop-x-0')
    acceptx.trigger('drop', { origin: 'x line' })
    expect(actions.acceptLineX.should.have.been.called)
  })

  it('has a working y drop zone', () => {
    const wrapper = mount(LineTarget, { store, localVue })
    const acceptx = wrapper.find('#drop-y-0')
    acceptx.trigger('drop', { origin: 'y line' })
    expect(actions.acceptLineY.should.have.been.called)
  })
})
