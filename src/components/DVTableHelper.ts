/* Prepare DataView table data from JSON object */

/* Defines a single row of table data */
class DVTableData {
  /* name of the presented variable */
  name: string;
  /* value, can be empty if the variable is a container */
  value: string|number|any;
  /* varies, indicates that the row is shown */
  open: number;
  /* nesting depth of the variable */
  level: number;
  /* points to the parent, next lower nested */
  parent: any;
  /* index in the row */
  index: number|any;

  /* Create with constructor */
  constructor (name: string, value: string|number|any,
    level: number, parent: any) {
    this.name = name
    this.value = value
    this.open = 0
    this.level = level
    this.parent = parent
  }
}

/* Convert a data struct to a type fit for a Bootstrap Vue table.

  @param obj    table data:   name, value keys
  @param res    resulting flat table to be extended
  @param level  current nesting level, function will be called recursively
  @param idx    row number
      */
const toTable = (obj: any, res: Array<DVTableData>,
                 level: number, idx: number) => {
  /* to keep track of level changes */
  let parentrow: any = null
  let nowlevel = 0

  const appendOrExtend = function (key: string, value: any, level: number) {
    if (idx < res.length) {
      if (res[idx].level === level && res[idx].name === key) {
        res[idx].value = value
        // console.log('toTable replace i-l-o-k-v:',
        // idx, level, res[idx].open, key, value)
        return
      } else {
        // console.log('no match', res[idx].level, level, res[idx].name, key)
        // console.log('toTable clip to', idx)
        res.length = idx
      }
    }
    // console.log('toTable push', key, 'at', res.length, 'level', level)
    if (level > nowlevel) {
      // console.log('with key', key, ' from level=', nowlevel, ' to ', level)
      parentrow = res[res.length - 1]
      nowlevel = level
    } else if (level < nowlevel) {
      while (parentrow !== null && parentrow.level >= level) {
        // console.log('up parent ', parentrow.key)
        parentrow = parentrow.parent
      }
      nowlevel = level
    }

    res.push(new DVTableData(key, value, level, parentrow))
    // console.log('toTable append i-l-k-v:', idx, level, key, value)
  }

  Object.entries(obj).forEach(([key, value]) => {
    if (value instanceof Object) {
      appendOrExtend(key, null, level)
      idx = toTable(value, res, level + 1, idx + 1)
    } else if (value instanceof Array) {
      appendOrExtend(key, null, level)
      for (let i = 0; i < value.length; i++) {
        idx = toTable({ i: value[i] }, res, level + 1, idx + 1)
      }
    } else {
      appendOrExtend(key, value, level)
      idx++
    }
  })
  if (res.length > idx + 1) {
    // console.log('final clip to', idx)
    res.length = idx + 1
  }
  let ii = 0
  for (const r of res) {
    r.index = ii++
  }
  return idx
}

export { DVTableData, toTable }
