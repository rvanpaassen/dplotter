/*
    PlotConfiguration.ts
    RvP 190228

    Set of classes defining configuration of plots, used in vuex store
    to keep, communicate and save/restore plot configuration
*/

import { dataCallbackFn,
  ChannelInfo } from '@/store/ChannelData'
import Vue from 'vue'


/** Global variable defining DUECA time tick size */
let timeGranule = 0.0001

class LineDataLink {
  // name of the endpoint
  endpoint: string;
  // entry Number
  entryId: number;
  // path to the data member, empty path means reading tick
  datapath: Array<string>;
  // data reader that collects the channel input
  reader: any;
  // callback function on the line, in the LineConfiguration object
  dataPassFunction: dataCallbackFn|null;
  // function to convert the values
  convValue: any;
  // offset value for line data function
  data_offset: number|any;

  // constructor
  constructor(location: string, entryId: number,
    datapath: Array<string>, passFunction: any) {
    this.endpoint = location
    this.entryId = entryId
    this.datapath = datapath
    // this.reader = reader
    this.dataPassFunction = passFunction
    this.data_offset = 0

    // creates the converter function from datapath specification
    this.convValue = this._lineFunction()

    // installs the callback client
    ChannelInfo.installCallbackClient(location, entryId, passFunction)
  }

  /** write the path as a pretty name for the line definition

      @returns        Readable representation, like mydata{0}:val
  */
 _writeName () {
    if (this.datapath.length) {
      return this.endpoint + '{' + this.entryId + '}' + ':data' +
        this._writePath().join('')
    } else {
      return this.endpoint + '{' + this.entryId + '}' + ':tick'
    }
  }

  /** write the path to a data member as an evaluable expression

    @param xline    names/indices of member variables
    @returns        selector, starting with a '.'
  */
  _writePath () {
    const sel: Array<string> = []
    for (const e of this.datapath) {
      if (isNaN(parseInt(e))) {
        sel.push('.' + e)
      } else {
        sel.push('[' + e + ']')
      }
    }
    return sel
  }

  /** iterate through the data structure, to arrive at a string that can be
      "eval"-ed to access the x or y data

    @returns        A function that reads data or reads&scales tick
  */
  _lineFunction () {
    if (this.datapath.length) {
      const fn = 'return x.data' + this._writePath().join('')
      // console.log('dynamic function', fn)
      /* eslint no-new-func: "off" */
      return Function('x', fn)
    } else {
      // console.log('dynamic function tick')
      // const fn = 'return '
      return (x: any) => {
        if (this.data_offset === null) { this.data_offset = x.tick }
        return timeGranule * (x.tick - this.data_offset)
      }
    }
  }

  /** Disconnect the callback */
  reset () {
    if (this.dataPassFunction) {
      ChannelInfo.removeCallbackClient(this.endpoint, this.entryId,
        this.dataPassFunction)
      }
    }
}

/** Different plot run modes. */
enum PlotRunMode {
  Running,
  ResumeRun,
  Paused,
  StopAndClear,
  BeingEdited
}

class LineConfiguration {
  // callback function that extends the data lines
  _cbExtend: any;
  // callback that clears the data lines
  _cbClear: any;
  // holding array for X values
  _xhold: Array<number>;
  // holding array for Y values
  _yhold: Array<number>;
  // label for the plot
  label: string;
  // running or not
  _running: PlotRunMode;
  // A link to the graphical representation
  _plset: any;
  // Number of points to keep in the line
  npoints: number;
  // x line
  xline: LineDataLink|null;
  // Readable
  xlineDisplay: string;
  // y line
  yline: LineDataLink|null;
  // human readable
  ylineDisplay: string;
  /// Offset X data on first datapoint
  offset_on_first: boolean;

  /** Create a new line configuration.

    @param npoints    Number of data points to keep in the line
    @param nchunk     Number of data points to assemble before updating the
                      line and plot
    @param lineno     Line number within this plot
    @param plotid     Numerical ID of the plot itself
    @param plset      A link to the graphical representation
  */
  constructor (npoints: number, nchunk: number,
    lineno: number, plotid: number, plset: any) {

    this._plset = plset

    // create a function that sends data to the plot's x, y arrays
    this._cbExtend = () => {
      // console.log(`cbextend nx ${this._xhold.length} ny ${this._yhold.length}`)

      if (this._xhold.length >= nchunk && this._yhold.length >= nchunk) {

        // console.log(`cbextend with chunk, mode ${this._running}`)
        if (this._running === PlotRunMode.Running) {

          // send an nchunk block
          // console.log(`cbextend emitting 'adding-data-to-${plotid}-${lineno}'`)
          this._plset.myEmit('adding-data-to-' + plotid + '-' + lineno,
            {
              x: [this._xhold.slice(0, nchunk)],
              y: [this._yhold.slice(0, nchunk)]
            })

          // remove from the holds
          this._xhold.splice(0, nchunk)
          this._yhold.splice(0, nchunk)

        } else if (this._running === PlotRunMode.ResumeRun) {

          // send an integer number of nchunk blocks
          const nchunk2 = Math.max(
            Math.floor(this._xhold.length/nchunk),
            Math.floor(this._yhold.length/nchunk)) * nchunk
          this._plset.myEmit('adding-data-to-' + plotid + '-' + lineno,
            {
              x: [this._xhold.slice(0, nchunk2)],
              y: [this._yhold.slice(0, nchunk2)]
            })

          // remove from the holds
          this._xhold.splice(0, nchunk2)
          this._yhold.splice(0, nchunk2)
          this._running = PlotRunMode.Running

        } else if (this._running === PlotRunMode.Paused ||
                   this._running === PlotRunMode.BeingEdited) {

          // keep saving, unless over max points
          if (this._xhold.length > this.npoints) {
            this._xhold.splice(0, this._xhold.length - this.npoints)
          }
          if (this._yhold.length > this.npoints) {
            this._yhold.splice(0, this._yhold.length - this.npoints)
          }

        } else { // StopAndClear
          // remove from the holds
          this._xhold.splice(0, nchunk)
          this._yhold.splice(0, nchunk)
        }
      }
    }

    // create the function to clear the line again
    this._cbClear = () => {
      this._plset.myEmit('clearing-data-from-' + plotid + '-' + lineno)
    }

    // initialize the data
    this._xhold = []
    this._yhold = []
    this.label = ''
    this._running = PlotRunMode.StopAndClear
    this.npoints = npoints
    this.xlineDisplay = 'not connected'
    this.ylineDisplay = 'not connected'
    this.offset_on_first = plset.offsetOnFirst
  }

  /** Set the global time scale.

    @param granule    Value of a single increment, in seconds
  */
  static setGranule (granule: number) {
    timeGranule = granule
  }

  /** callback function, for collecting new X-line data

    @param data       New data object coming in. The _convXvalue function
                      will select the relevant data item.
  */
  _addX = (data: any) => {
    // console.log('add X data')
    if (data === null) {
      // truncate all
    } else if (this._running <= PlotRunMode.Paused) {
      this._xhold.push(this.xline?.convValue(data))
    }
    this._cbExtend()
  }

  /** callback function for collecting new Y-line data

    @param data       New data object coming in. The _convXvalue function
                      will select the relevant data item.
  */
  _addY = (data: any) => {
    // console.log('add Y data')
    if (data === null) {
      // truncate all
    } else if (this._running <= PlotRunMode.Paused) {
      this._yhold.push(this.yline?.convValue(data))
    }
    this._cbExtend()
  }

  /** Specify the x-axis data for the plot

    @param xline      array of member names/indices
    @param doset      if true, set, otherwise clear
    @param endpoint   endpoint on the server/reader label
    @param entryId    entry ID
  */
  setXLine = (xline: Array<string>, doset: boolean,
    endpoint: string, entryId: number) => {
    if (this.xline) {
      this.xline.reset()
    }
    if (doset) {
      this.xline = new LineDataLink(endpoint, entryId, xline, this._addX)
      this.xlineDisplay = this.xline._writeName()
      if (this.offset_on_first) {
        this.xline.data_offset = null
      }
    } else {
      this.xlineDisplay = 'not connected'
    }
  }

  /** Specify the x-axis data for the plot

    @param yline      array of member names/indices
    @param rdr        reader associated with the data object
    @param endpoint   endpoint on the server/reader label
    @param entryId    entry ID
  */
  setYLine = (yline: Array<string>, doset: boolean,
    endpoint: string, entryId: number) => {
    if (this.yline) {
      this.yline.reset()
    }
    if (doset) {
      this.yline = new LineDataLink(endpoint, entryId, yline, this._addY)
      this.ylineDisplay = this.yline._writeName()
    }
  }

  /** Check whether x and y data both specified

    @returns            true if correct
  */
  isComplete = () => {
    return this.xline !== null && this.yline !== null
  }

  /** Toggle between following data and off/configuration

    @param flag         flag indicating run state
  */
  setRunning = (flag: PlotRunMode) => {
    if (this.isComplete()) {
      // start collecting data and passing it on
      this._running = flag
    }
    if (flag === PlotRunMode.StopAndClear) {
      // clear the current data. Hold is the current line
      this._xhold.length = 0
      this._yhold.length = 0
      if (this.xline && this.offset_on_first) {
        this.xline.data_offset = null
      }
      this._cbClear()
    }
  }

  /** Unset the line and its connections */
  reset= () => {
    this.setRunning(PlotRunMode.StopAndClear)
    if (this.xline) {
      this.xline.reset()
      this.xline = null
    }
    if (this.yline) {
      this.yline.reset()
      this.yline = null
    }
    this.label = ''
  }
}


class PlotConfiguration {
  /// Lines defined in this plot
  lines: Array<LineConfiguration>
  /// Descriptive title
  title: string
  /// Layout object, currently unused
  layout: Record<string, any>
  /// Options object, currently unused
  options: Record<string, any>
  /// unique plot ID, for emitting messages etc.
  plotid: number
  /// reference to the client object
  plotclient: any
  /// Hold up area for instructions to the client object
  stashed_emits: Array<[string,any]>
  /// pane for the client
  runtab: number;
  /// link to the plot control object, if used
  plotcontrol: LineDataLink|any
  /// Name of the plotcontrol link
  plotcontrolDisplay: string
  /// Use offset on time Lines
  offsetOnFirst: boolean

  constructor (plotid: number) {
    this.lines = []
    this.layout = { }
    this.options = { }
    this.plotclient = null
    this.title = ''
    this.plotid = plotid
    this.stashed_emits = []
    this.runtab = 2 // stopped
    this.plotcontrol = null
    this.plotcontrolDisplay = ''
    this.offsetOnFirst = false
  }

  /** Set lines to reset time data on a reset of the plot lines

      @param choice     If true, ...
  */
  setOffsetOnFirst(choice: boolean) {
    this.offsetOnFirst = choice
    for (const l of this.lines) {
      l.offset_on_first = choice
    }
  }


  /** Add a line.

    @param npoints     Number of points to keep
    @param nchunk      Chunk sizes for updating graph
    @param plotid      Plot id reference
   */
  addLine = (npoints: number, nchunk: number) => {
    const lineno: number = this.lines.length
    // console.log('adding a line number', lineno, npoints, nchunk)
    if (lineno === 0 || this.lines[lineno - 1].isComplete()) {
      Vue.set(this.lines, lineno,
        new LineConfiguration(npoints, nchunk, lineno,
          this.plotid, this))
      // console.log('emitting on add-line-to-' + this.plotid)
      this.myEmit('add-line-to-' + this.plotid,
        { npoints: npoints, lineno: lineno, name: this.lines[lineno].label })
    } else {
      throw (new RangeError('First complete previous line'))
    }
  }

  /* Set running state */
  setRunning (flag: PlotRunMode) {

    if (flag === PlotRunMode.ResumeRun || flag === PlotRunMode.Running) {
      this.runtab = 0
    } else {
      this.runtab = flag - 1
    }
    // ensure all lines respond to the run mode
    this.lines.forEach((l) => {
      l.setRunning(flag)
    })
  }

  /* Set the plot client */
  setPlotClient (client: any) {
    this.plotclient = client
    let e = this.stashed_emits.shift()
    while (e) {
      // console.log(`emitting stashed ${e[0]}`)
      this.plotclient.$emit(e[0], e[1])
      e = this.stashed_emits.shift()
    }
  }

  /* emit message or stash it */
  myEmit(msg: string, data: any) {
    if (this.plotclient) {
      this.plotclient.$emit(msg, data)
    } else {
      // console.log(`stashing ${msg}`)
      this.stashed_emits[this.stashed_emits.length] = [msg, data]
    }
  }

  /* reset, undo */
  reset () {
    this.lines.forEach( (l) => {
      l.reset()
    })
    this.lines.length = 0
  }

  /** Callback function for reacting to the control link

    @param data     Data from channel reading
  */
  _plotControl= (data: any) => {
    // console.log('plot ${this.plotid} control call', data)
    if (this.runtab === 3) {
      // console.log(`Plot ${this.plotid} being edited, ignoring remote control`)
    } else {
      const cmd = this.plotcontrol?.convValue(data)
      if (cmd === 0) {
        this.setRunning(PlotRunMode.Paused)
      } else if (cmd === 1) {
        this.setRunning(PlotRunMode.ResumeRun)
      } else {
        this.setRunning(PlotRunMode.StopAndClear)
      }
    }
  }

  /** Install a data-driven control link.

    @param cpath      array of member names/indices
    @param doset      if true, set, otherwise clear
    @param endpoint   endpoint on the server/reader label
    @param entryId    entry ID
  */
  setControl (cpath: Array<string>, doset: boolean,
    endpoint: string, entryId: number) {
    if (this.plotcontrol) {
      this.plotcontrol.reset()
    }
    if (doset) {
      this.plotcontrol = new LineDataLink(endpoint, entryId,
        cpath, this._plotControl)
      this.plotcontrolDisplay = this.plotcontrol._writeName()
    } else {
      this.plotcontrolDisplay = 'not connected'
    }
  }

}

export { LineConfiguration, PlotConfiguration, PlotRunMode }
