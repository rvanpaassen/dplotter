/* ChannelData
   RvP 190228

   Set of classes to capture channel information and data from the
   DUECA server; used in the vuex store
*/
import Vue from 'vue'

/* callback function type */
type dataCallbackFn = (d: any) => void;


/* The ChannelEntryReader contains current data, socket and list of clients
*/
class ChannelEntryReader {
  // a list of callbacks to the plot configurations that collect the data
  clients: Array<dataCallbackFn>;
  // connection to DUECA
  socket: WebSocket|any;
  // current data point, as shown in the "Connect" tab
  data: {
    tick: number;
    data: any;
  };

  // URL for the connection
  url: string|any;
  // entry ID in channel
  entryId: number;

  /* create a new one, in most cases initially unconnected */
  constructor (sk: WebSocket|any = null) {
    this.data = {
      tick: 0,
      data: {}
    }
    this.socket = sk
    this.clients = []
    this.url = ''
    this.entryId = 0
  }


  /* reset for re-use, as when the entry disappears, or connection is closed */
  _reset () {
    // all clients get called with null, to indicate end of connection
    this.clients.forEach(function (f) { f(null) })
    // clean up
    this.socket = null
    this.clients.length = 0
    this.data.tick = 0
    this.data.data = {}
    // remove from the array


    this.url = null
  }

  /*
    message handler for the websocket
  */
  _onmessage= (event: any) => {
    // console.log(`data message ${event} for ${this.clients.length} clients`)
    try {
      const data = JSON.parse(event.data)
      Vue.set(this.data, 'tick', data.tick)
      Vue.set(this.data, 'data', data.data)

      // notify all clients
      this.clients.forEach(function (f) { f(data) })
    }
    catch(err) {
      console.warn('Error decoding or transmitting received data :', err,
        ' with: ', event.data)
    }
  }

  /*
    error handler for the data websocket
  */
  _onerror= (event: any) => {
    console.warn('Data read error', event, this.url)
    this._reset()
  }

  /*
    close handler for the data WebSocket
  */
  _onclose= (event: any) => {
    console.warn('Data socket close', event, this.url)
    this._reset()
  }

  /*
    connect to the data source; the data source is under the
    followdata url. This URL is connected to a single channel; The
    entry id is coded in the first data message sent
  */
  connect= (baseUrl: string, url: string, entryid: number) => {
    this.url = url
    this.entryId = entryid
    this.socket = new WebSocket(
      baseUrl + 'read/' + url + '?entry=' + entryid)

    // connect the callbacks
    this.socket.onmessage = this._onmessage
    this.socket.onerror = this._onerror
    this.socket.onclose = this._onclose

    // console.log('created data connection to', baseUrl, url, entryid)
  }

  /*
    Disconnect from the data source.
  */
  disconnect= () => {
    if (this.socket) {
      // console.log('Closing socket on ' + this.url)
      this.socket.close()
    }
  }

  /*
    Add a callback
  */
  addCallback (fcn: dataCallbackFn) {
    this.clients.push(fcn)
  }

  /*
    Remove a callback
  */
  removeCallback (fcn: dataCallbackFn) {
    const idx = this.clients.findIndex(fcn)
    if (idx >= 0) {
      this.clients.splice(idx)
    }
  }
}

/** Contain information about the entries.

  Optionally, the reader is set; meaning that the entry is connected
  */
class ChannelEntryInfo {
  config: any;
  /** For the interface, indicate that the data view is opened */
  showEntryDetail: boolean;
  /** Accesses the entry data */
  reader: ChannelEntryReader|null;

  constructor (config: any) {
    this.config = config
    this.showEntryDetail = false
    this.reader = new ChannelEntryReader()
  }

  disconnect () {
    this.config = { }
    this.showEntryDetail = false
    if (this.reader !== null) {
      this.reader.disconnect()
    }
    this.reader = null
  }
}

type CallbackMap= Map<string,Array<dataCallbackFn> >

/*  The "channelinfo" array contains all connection data.
*/
class ChannelInfo {
  url: string;
  socket: WebSocket|any;
  entries: Array<ChannelEntryInfo>;
  presets: Array<number>;
  baseurl: string;

  /** map of all active ChannelEntryReader objects */
  static _infoList: Map<string,ChannelInfo> = new Map<string,ChannelInfo>();

  /** Collection of all still unconnected callback requests */
  static _waitingCallbacks: CallbackMap = new Map();

  constructor (url: string) {
    this.url = url
    this.entries = []
    this.socket = null
    this.presets = []
    this.baseurl = ''
    ChannelInfo._infoList.set(url, this)
  }

  /** Look for a reader matching endpoint and entry ID.

    @param url        Endpoint URL
    @param entryId    Entry ID
    @returns          The reader
   */
  static getChannelEntryReader(url: string, entryId: number) {
    return ChannelInfo._infoList.get(url)?.entries[entryId]?.reader
  }

  /** Install a callback with the proper reader, or leave in wait area
    @param url          Endpoint url
    @param entryid      Entry /ID
    @param cbfun        Callback Function
    @returns            True if installed, false if _waiting
  */
  static installCallbackClient(url:string, entryId: number,
      cbfun: dataCallbackFn) {
    let rdr = ChannelInfo._infoList.get(url)?.entries[entryId]?.reader
    if (rdr) {
      // console.log(`direct install callback on ${url}:${entryId}`)
      rdr.addCallback(cbfun)
      return true
    } else {
      // console.log(`waiting callback on ${url}:${entryId}`)
      const key:string = url + '#' + entryId
      let cblist = ChannelInfo._waitingCallbacks.get(key)
      if (!cblist) {
        // console.log(`new waiting cb list`)
        cblist = []
        ChannelInfo._waitingCallbacks.set(key, cblist)
      }
      cblist.push(cbfun)
      // for (let k of ChannelInfo._waitingCallbacks.keys()) {
        // console.log(`have now keys ${k}`)
      // }
      return false
    }
  }

  /** Remove a callback either from reader or wait area
    @param url          Endpoint url
    @param entryId      Entry /ID
    @param cbfun        Callback Function
    @returns            True if installed, false if _waiting
  */

  static removeCallbackClient(url:string, entryId: number,
      cbfun: dataCallbackFn) {
    let rdr = ChannelInfo._infoList.get(url)?.entries[entryId]?.reader
    if (rdr) {
      // console.log(`effective remove callback from ${url}:${entryId}`)
      rdr.removeCallback(cbfun)
      return
    }
    const key: string = url + '#' + entryId
    let cblist = ChannelInfo._waitingCallbacks.get(key)
    if (cblist) {
      cblist.forEach( (cb: dataCallbackFn, index: number) => {
        if (cb === cbfun) {
          // console.log(`effective remove callback from ${url}:${entryId}`)
          cblist?.splice(index, 1)
          return
        }
      })
    }
    console.warn(`Could not remove callback function for ${url}:${entryId}`)
  }

  /** Unset all connections, remove all entry information and reading */
  reset= () => {
    if (this.socket !== null) {
      for (const e of this.entries) {
        e.disconnect()
      }
      this.entries.length = 0
      this.socket = null
    }
  }

  /*
    message handler, data contains JSON indicating new or deleted entry
  */
  _onmessage= (event: any) => {
    const data = JSON.parse(event.data)

    if (data.dataclass.length) {

      // when dataclass is non-empty, indicates new entry
      // add the entry and make it accessible to vue
      let idx: number = this.presets.length ? this.presets[0] : -2
      let entryId: number = this.entries.length
      Vue.set(this.entries, this.entries.length,
        new ChannelEntryInfo(data))

      // checking against callbacks requested
      const key = this.url + '#' + entryId
      let cbreq = ChannelInfo._waitingCallbacks.get(key)
      if (cbreq) {
        // console.log(`effectuating callbacks on ${this.url}:`)
        for (const cb of cbreq) {
          this.entries[entryId].reader?.addCallback(cb)
        }
        ChannelInfo._waitingCallbacks.delete(key)
      }

      // checking against presets for connect
      // console.log('checking against preset ', idx)
      if (this.entries.length - 1 === idx) {
        this.entries[idx].reader?.connect(
          this.baseurl, this.url, idx)
        this.presets.shift()
      }

    } else {
      // when dataclass is empty, removal of entry
      const idx = this.entries.findIndex(function (e) {
        return e.config.entry === data.entry
      })
      if (idx > -1) {
        // console.log('removing entry at', idx, 'data', this.entries[idx])

        // this.$delete(b.entries, idx)
        Vue.delete(this.entries, idx)
        // console.log('remaining', this.entries)
      } else {
        console.warn('cannot remove entry', data)
      }
    }
  }

  /*
    error handler
  */
  _onerror= (event: any) => {
    // console.log('websocket error', event, this.url)
    this.reset()
  }

  /*
    close of the entry information websocket
  */
  _onclose= (event: any) => {
    // console.log('websocket closing', event, this.url)
    this.reset()
  }

  /*
    connect to the information websocket
  */
  connect= (baseurl: string) => {
    this.socket = new WebSocket(baseurl + 'info/' + this.url)
    this.baseurl = baseurl
    // the socket sends JSON messages with channel config;
    // { entry: number, dataclass: string, typeinfo: any }

    // new messages; each message defines an entry with its structure
    this.socket.onmessage = this._onmessage
    this.socket.onerror = this._onerror
    this.socket.onclose = this._onclose
  }

  presetEntries= (entries: Array<number>) => {
    // console.log('preset entries ' + entries)
    this.presets = entries
  }

  disconnect= () => {
    if (this.socket) {
      this.socket.close()
    }
  }
}

export { ChannelEntryReader, ChannelEntryInfo, ChannelInfo, dataCallbackFn }
