/* ConfigSaving.ts
   RvP 200619

   Set of classes for getting configuration files from the dueca
   server process
*/
import Vue from 'vue'
import store from '@/store/Store.vue'

interface SConfigChannelInfo {
  name: string;
  entries: Array<number>;
}

interface SConfigLink {
  endpoint: string;
  entryId: number;
  datapath: Array<string>;
}

interface SConfigLine {
  xline: SConfigLink|null;
  yline: SConfigLink|null;
  label: string;
  npoints: number;
}

interface SConfigPlot {
  title: string;
  lines: Array<SConfigLine>;
  plotcontrol: SConfigLink|null;
  offsetOnFirst: boolean;
}

/* Content of a configuration file */
interface SConfigData {
  url: string
  channels: Array<SConfigChannelInfo>
  plots: Array<SConfigPlot>
}

/* Describe an entry in the list of available config files */
interface NameSizeDate {
  name: string;
  size: number;
  data: string;
}

class ConfigSocketSet {
  socket: WebSocket|null;
  initialized: boolean;
  configFiles: Array<NameSizeDate>;
  _name: string;

  constructor () {
    this.socket = null
    this.initialized = false
    this.configFiles = []
    this._name = 'ConfigSocketSet'
  }

  /* communication pattern:
     - socket is opened
     - upon opening, send a 1st message with the datatype to write;
       {dataclass: 'ConfigFileRequest'}
     - the response has information on the write and read directions;
       {write: {dataclass: 'ConfigFileRequest', entry: .., typinfo: {..}},
        read: {dataclass: 'ConfigFileData', entry: .., typinfo: {..}}}
     - after that, writes contain a struct with a member 'data', conforming
       to the ConfigFileRequest
       {data: {}}
     - replies contain a struct with members 'data' and 'tick'
   */
  _requestOpen= () => {

    // initialize
    // console.log(this._name + ' sending dataclass')
    if (this.socket === null) {
      // console.warn('plotconfig socket is gone')
      return
    }
    this.socket.send(JSON.stringify({
      dataclass: 'ConfigFileRequest'
    }))
  }

  _requestClose= (event: any) => {
    // console.log(this._name + ' closing: ' + event.reason)
    this.socket = null
  }
  _requestError= (event: any) => {
    console.error(this._name + ' error: ' + String(event))
  }
  _requestMessage= (event: any) => {
    let data = JSON.parse(event.data)
    let ii: number
    // console.log(`plotconfig response ${data}`)
    if (this.initialized) {
      if (data.data.config.length == 0) {
        // new list of files
        this.configFiles.length = 0
        for (ii = 0; ii < data.data.filenames.length; ii++) {
          Vue.set(this.configFiles, ii, data.data.filenames[ii])
        }
        // console.log(this.configFiles)
      }
      else {
        // new configuration
        try {
          let newconf: SConfigData = JSON.parse(data.data.config)
          store.commit('restoreConfiguration', newconf)
        } catch (error) {
          console.error('parsing config data', error)
        }
      }
    } else {
      // first message, should give me info on comms
      if (data.write.dataclass != 'ConfigFileRequest' ||
          data.read.dataclass != 'ConfigFileData') {
        console.error(`Unexpected communication type ${data}`)
      } else {
        // send off a request for list of config files
        if (this.socket) {
          this.socket.send(JSON.stringify({
            data: { name: '', config: ''}
          }))
          this.initialized = true
        } else {
          console.error('configfiles socket not available')
        }
      }
      this.initialized = true
    }
  }

  close= () => {
    if (this.socket) {
      this.socket.close()
      this.socket = null
      this.initialized = false
      this.configFiles.length = 0
    }
  }

  open= (base: string) => {
    if (this.socket) {
      this.socket.close()
    }
    // console.log(`opening plotconfig at ${base}write-and-read/plotconfig`)
    this.socket = new WebSocket(base + 'write-and-read/plotconfig')
    this.socket.onopen = this._requestOpen
    this.socket.onerror = this._requestError
    this.socket.onclose = this._requestClose
    this.socket.onmessage = this._requestMessage
  }

  loadConfiguration= (config: string) => {
    if (!this.socket) return
    this.socket.send(JSON.stringify({
      data: { name: config, config: '' }
    }))
  }

  saveConfiguration= (name: string, config: SConfigData) => {
    if (!this.socket) return
      this.socket.send(JSON.stringify({
      data: { name: name, config: JSON.stringify(config) }
    }))
  }
}

export { NameSizeDate, ConfigSocketSet,
  SConfigData, SConfigPlot, SConfigChannelInfo, SConfigLine }
