module.exports = {
  parser: 'vue-eslint-parser',
  parserOptions: {
    ecmaVersion: 2017,
    parser: 'babel-eslint',
    sourceType: 'module',
    ecmaFeatures: {
      "legacyDecorators": true
    },
  },
  extends: [
    'plugin:vue/base'
  ],
  plugins: [
    'vue'
  ],
  env: {
    browser: true,
    node: true,
    mocha: true
  },
  globals: {
    expect: true
  },
  rules: {
    'no-console': 'warn',
    'vue/max-attributes-per-line': [
      1,
      {
        'singleline': 3,
        'multiline': 1
      }
    ],
    'vue/html-indent': [
      'warn',
      2,
      {
        'attribute': 1,
        'baseIndent': 1,
        'closeBracket': 0,
        'alignAttributesVertically': true,
        'ignores': []
      }
    ],
    'vue/component-name-in-template-casing': 'off'
  },
  overrides: [
    {
      files: ['*.ts', '*.tsx'],
      parser: '@typescript-eslint/parser',
      rules: {
	       'no-undef': 0,
         'no-unused-components': 0
      }
    }
  ]
}
