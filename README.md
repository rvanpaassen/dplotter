# plotter

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### Good resources
[devdocs](https://devdocs.io/vue~2-guide/)

[class-based components](https://github.com/kaorun343/vue-property-decorator)

[class-based interaction vuex](https://github.com/ktsn/vuex-class/)

[bootstrap vue](https://bootstrap-vue.org/)

[bootstrap formatting](https://getbootstrap.com/)

[fontawesome](https://fontawesome.com/icons?d=gallery&m=free)

[vue fontawesome](https://github.com/FortAwesome/vue-fontawesome)

[plotly currently used](https://github.com/statnett/vue-plotly)

[another plotly option](https://github.com/David-Desmaisons/vue-plotly)

[drag and drop](https://github.com/cameronhimself/vue-drag-drop)
