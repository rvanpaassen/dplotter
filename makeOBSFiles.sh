#!/bin/bash
# Make a package tarball from svn

# usage message if no arguments
function usage()
{
    echo "Usage: `basename $0` [options] VERSION"
    echo "Options:"
    echo "   -h     use git master"
    echo "   -H ARG use alternative git branch"
    echo "   -k     keep temporary dir"
    exit 1
}

# defaults
SCRIPTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
TARGETDIR=`pwd`
NAME=dplotter
PKGDIR=`mktemp -d /tmp/${NAME}.XXXXXXXX`
KEEPTMP=
OSCDIR="${HOME}/rpmbuild/home:repabuild/${NAME}"

# root of the repository with source
GITSERVER=$(git remote -v | grep '^origin.*[(]fetch[)]$' | \
                sed -e 's/origin[[:space:]]*\([^[:space:]]*\).*$/\1/')
GITREMOTE="--remote=${GITSERVER}"

# process input arguments
while getopts "khH:s:" optname
do
  case $optname in
      h) GITBRANCHORTAG=master; GITREMOTE="" ;;
      H) GITBRANCHORTAG="$OPTARG"; GITREMOTE="" ;;
      k) KEEPTMP=1 ;;
    [?]) usage
            exit 1;;
    esac
done
shift `expr $OPTIND - 1`
if test -z $1; then
  usage
  exit 1;
fi
VERSION=$1
if [ -z "$GITBRANCHORTAG" ]; then
    GITBRANCHORTAG=$VERSION
fi

# create a source set
function create_srcfiles()
{
    echo "Using temporary dir $PKGDIR"
    mkdir ${PKGDIR}/${NAME}-${VERSION}
    git archive --format=tar \
        $GITREMOTE ${GITBRANCHORTAG} | \
        tar -C $PKGDIR/${NAME}-${VERSION} -xf -

    pushd $PKGDIR
    pushd ${NAME}-${VERSION}

    if [ `pwd` != "$PKGDIR/${NAME}-${VERSION}" ]; then
        echo "Not operating in the right location"
        exit 1
    fi

    cmake .
    make webcode icons
    pushd obs
    cp $NAME.spec ../..
    cp $NAME.dsc ../..
    cp Portfile ../..
    tar cvf ../../debian.tar debian
    popd
    rm -rf node_modules
    rm -f CMakeCache.txt
    rm -rf CMakeFiles
    popd

    tar cfj ${NAME}-${VERSION}.tar.bz2 ${NAME}-${VERSION}
    rm -rf ${NAME}-${VERSION}
    FILES=`ls *`

    # OS X macports
    if [ -d $HOME/ports/science/${NAME} ]; then
        sha256=`openssl dgst -sha256 ${NAME}-${VERSION}.tar.bz2 | cut -f2 -d' '`
        rmd160=`openssl dgst -ripemd160 ${NAME}-${VERSION}.tar.bz2 | cut -f2 -d' '`
        sed -e "s/RMD160/${rmd160}/
                s/SHA256/${sha256}/" -i.bak Portfile
        cp -f Portfile $HOME/ports/science/$NAME
        cp -f ${NAME}-${VERSION}.tar.bz2 $HOME/ports/science/${NAME}/files
        echo "Created portfile"
    fi

    if [ -z "$KEEPTMP" -a -d "${OSCDIR}" ]; then
        mv -f $FILES "${OSCDIR}"
        echo "Copied"
        echo $FILES
        echo "to ${OSCDIR}"
        popd
        rm -rf $PKGDIR
    else
        popd
        echo "Created files $FILES"
        echo "You can find them in $PKGDIR"
    fi
}

create_srcfiles
